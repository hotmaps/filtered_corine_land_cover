# filtered_corine_land_cover_with_id


## Repository structure
```
data                    -- containts the dataset in Geotiff format
readme.md               -- Readme file
datapackage.json        -- Includes the meta information of the dataset for processing and data integration

```


## Documentation

This dataset provides areas identified for work, with ID corresponding to their urban area.
The working area is based on the corine land cover dataset [1] filtered on the folowing categories:

```
0	1	111	Continuous urban fabric	230-000-077
1	2	112	Discontinuous urban fabric	255-000-000
2	3	121	Industrial or commercial units	204-077-242
```
The pixel values are 1 on in the selected areas and 0 outside.

## References

[1] [Corine Land Cover](https://land.copernicus.eu/pan-european/corine-land-cover/clc2018)


## How to cite
Jeannin Noémie, OPENGIS4ET Project WP3


## Authors
Jeannin Noémie <sup>*</sup>

<sup>*</sup> [EPFL, PV-Lab](https://www.epfl.ch/labs/pvlab/), Laboratory of photovoltaics and thin-films electronics, Rue de la Maladière 71b, CH-2002 Neuchâtel 2


## License
Copyright © 2023: Noémie Jeannin

Creative Commons Attribution 4.0 International License
This work is licensed under a Creative Commons CC BY 4.0 International License.

SPDX-License-Identifier: CC-BY-4.0

License-Text: https://spdx.org/licenses/CC-BY-4.0.html

## Acknowledgment
We would like to convey our deepest appreciation to the OPENGIS4ET Project, which provided the funding to carry out the present investigation.